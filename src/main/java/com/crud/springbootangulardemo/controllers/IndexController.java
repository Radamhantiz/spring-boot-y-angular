package com.crud.springbootangulardemo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * IndexController
 */
@Controller
public class IndexController {

    @RequestMapping("/")
    public String getIndexPage() {
        return "index";
    }
}