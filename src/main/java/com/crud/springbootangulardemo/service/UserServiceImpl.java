package com.crud.springbootangulardemo.service;

import java.util.List;

import com.crud.springbootangulardemo.dao.UserRepository;
import com.crud.springbootangulardemo.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    protected UserRepository userRepository;

    @Override
    public User save(User user) {
        return this.userRepository.save(user);
    }

    @Override
    public List<User> findAll() {
		return this.userRepository.findAll();
	}

    @Override
    public void deleteUser(Long id) {
        this.userRepository.deleteById(id);
    }
}